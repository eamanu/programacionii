from django.db import models


class Personas(models.Model):
    name = models.CharField(max_length=25)
    surname = models.CharField(max_length=25)
    username = models.CharField(max_length=10)
    age = models.IntegerField()
    materia = models.ManyToManyField('Materias')


class Materias(models.Model):
    name = models.CharField(max_length=255)
    note = models.FloatField()

class Docente(models.Model):
    legajo = models.CharField(max_length=255)
